/*
** util.c for defonce in /home/da-sil_l/Depos/rush1
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Sun Mar 15 17:31:47 2015 Clement Da Silva
** Last update Sun Mar 15 17:38:54 2015 Clement Da Silva
*/

#include <stdlib.h>
#include <unistd.h>
#include "elcrypt.h"

char	*revstr(char *buffer)
{
  char	*rev;

  if ((rev = malloc(9 * sizeof(char))) == NULL)
    exit(1);
  rev[8] = 0;
  rev[0] = buffer[7];
  rev[1] = buffer[6];
  rev[2] = buffer[5];
  rev[3] = buffer[4];
  rev[4] = buffer[3];
  rev[5] = buffer[2];
  rev[6] = buffer[1];
  rev[7] = buffer[0];
  return (rev);
}

char	*ltc_tab(char *rev, unsigned long long int salamandre)
{
  int	i;
  int	n;

  i = 7;
  n = 0;
  while (i >= 0)
    {
      rev[n] = *((unsigned char *)&salamandre + i);
      --i;
      ++n;
    }
  return (rev);
}

void		put_padding()
{
  exit(1);
}
