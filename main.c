/*
** main.c for rush in /home/barbis_j/Documents/Projets/rush1
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Sat Mar 14 14:16:44 2015 Joseph Barbisan
** Last update Sun Mar 15 17:50:54 2015 Clement Da Silva
*/

#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "elcrypt.h"

void printBits(size_t const size, void const * const ptr)
{
  unsigned char *b = (unsigned char*) ptr;
  unsigned char byte;
  int i, j;

  for (i = size - 1; i >= 0; i--)
    {
      for (j = 7; j >= 0; j--)
        {
	  byte = b[i] & (1<<j);
	  byte >>= j;
	  printf("%u", byte);
        }
    }
  puts("");
}

void	fill_args(char **av, t_data *data)
{
  int	i;

  i = 0;
  bzero(data, sizeof(*data));
  while (av[i])
    {
      if (!strcmp(av[i], "-f") && av[i + 1])
	data->in_file = av[i + 1];
      else if (!strcmp(av[i], "-o") && av[i + 1])
	data->out_file = av[i + 1];
      else if (!strcmp(av[i], "-k") && av[i + 1])
	data->key = av[i + 1];
      else if (!strcmp(av[i], "-e"))
	data->method = ENCRYPT;
      else if (!strcmp(av[i], "-d"))
	data->method = DECRYPT;
      ++i;
    }
}

int	open_files(t_data *data)
{
  if ((data->infd = open(data->in_file, O_RDONLY)) == -1)
    {
      perror("Open");
      return (-1);
    }
  if ((data->outfd = open(data->out_file, O_CREAT | O_TRUNC | O_WRONLY,
			  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1)
    {
      perror("Open");
      return (-1);
    }
  if (data->method == 0 || data->key == NULL)
    {
      fprintf(stderr, "Method/key is missing\n");
      close(data->infd);
      close(data->outfd);
      return (-1);
    }
  return (0);
}

int	find_base(char **str)
{
  int	i;

  i = 0;
  if (!strncmp(*str, "0x", 2))
    return (16);
  if (!strncmp(*str, "%", 1))
    {
      *str = &(*str)[1];
      return (2);
    }
  while ((*str)[i])
    {
      if ((*str)[i] < '0' || (*str)[i] > '9')
	return (-1);
      ++i;
    }
  return (10);
}

int				main(int ac, char **av)
{
  t_data			data;
  unsigned long long int	lkey;
  int				base;

  if (ac != 8)
    {
      fprintf(stderr, "Usage: %s -[d/e] -f [in file] -o [out file] -k [key]\n",
	      av[0]);
      return (0);
    }
  fill_args(av, &data);
  if (open_files(&data) == -1)
    return (0);
  if ((base = find_base(&data.key)) == -1)
    lkey = atoll(data.key);
  else
    lkey = strtoll(data.key, NULL, base);
  lkey = key_gen(&lkey);
  algo_exec(&data, lkey);
  return (1);
}
