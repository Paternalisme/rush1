/*
** elcrypt.h for elfk in /home/da-sil_l/Depos/rush1
** .
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Sat Mar 14 14:58:37 2015 Clement Da Silva
** Last update Sun Mar 15 17:47:08 2015 Clement Da Silva
*/

#ifndef ELCRYPT_H_
# define ELCRYPT_H_

# define ENCRYPT	45
# define DECRYPT	56

typedef struct	s_data
{
  char		*in_file;
  char		*out_file;
  char		*key;
  int		infd;
  int		outfd;
  char		method;
}		t_data;

char		*revstr(char *);
char		*ltc_tab(char *, unsigned long long int);
void		put_padding();
int		algo_exec(t_data *, long long int);
unsigned int   	second_key_gen(unsigned long long int, unsigned int);
void		showbits(unsigned char);
long long int	key_gen(void *);

#endif /* !ELCRYPT_H_ */
