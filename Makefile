##
## Makefile for mlerk in /home/da-sil_l/Depos/rush1
## 
## Made by Clement Da Silva
## Login   <da-sil_l@epitech.net>
## 
## Started on  Sat Mar 14 14:30:36 2015 Clement Da Silva
## Last update Sun Mar 15 17:35:01 2015 Clement Da Silva
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -W -Wall -ansi -pedantic -std=c99

NAME	= elcrypt

SRC	= main.c		\
	  algo.c		\
	  util.c		\
	  key.c

OBJ	= $(SRC:.c=.o)


all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
