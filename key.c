/*
** key.c for rush in /home/barbis_j/Documents/Projets/rush1
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Sat Mar 14 14:31:30 2015 Joseph Barbisan
** Last update Sun Mar 15 17:49:00 2015 Clement Da Silva
*/

#include <stdio.h>
#include <string.h>
#include "elcrypt.h"

void		ltc_tab_bis(unsigned char k[], void *key)
{
  int		i;
  int		n;

  i = 7;
  n = 0;
  while (i >= 0)
    {
      k[n] = *((unsigned char *)key + i);
      --i;
      ++n;
    }
}

long long int	key_gen(void *key)
{
  unsigned char	tmp;
  unsigned char	k[8];
  int		i;
  int		n;
  long long int	lkey;

  ltc_tab_bis(k, key);
  i = 0;
  n = 7;
  while (i < 8)
    {
      k[i] = k[i] << i;
      k[i] = k[i] >> (i + 1);
      k[i] = k[i] << (i + 1);
      tmp = k[i + 1] >> n;
      k[i] = k[i] | tmp;
      --n;
      ++i;
    }
  memcpy(&lkey, k, 7);
  return (lkey);
}

unsigned int			second_key_gen(unsigned long long int key, unsigned int tour)
{
  unsigned int			i;
  unsigned long long int	*tmp;
  unsigned long long int	tmpkey;
  unsigned int			lul;

  i = 0;
  while (i < tour)
    {
      tmpkey = key << 52;
      tmpkey = tmpkey >> 8;
      key = key >> 12;
      key = key ^ tmpkey;
      key = key << 8;
      ++i;
    }
  tmp = &key;
  *tmp = *tmp >> 8;
  lul = *((int *)(tmp));
  return (lul);
}
