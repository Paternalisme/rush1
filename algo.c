/*
** algo.c for rush in /home/barbis_j/Documents/Projets/rush1
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Sat Mar 14 18:08:25 2015 Joseph Barbisan
** Last update Sun Mar 15 17:33:05 2015 Clement Da Silva
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "elcrypt.h"

unsigned int	feistel(unsigned int key, unsigned int block)
{
  return (key ^ block);
}

unsigned long long int		algo_loop_decrypt(long long int primary,
						  long long int *block)
{
  unsigned int			left;
  unsigned int			right;
  unsigned long long int	left_tmp;
  unsigned long long int	right_tmp;
  int				tour;
  unsigned long long int	ret;

  tour = 8;
  right = *((int *)block);
  left = *((int *)block + 1);
  while (tour >= 2)
    {
      left = left ^ feistel(second_key_gen(primary, tour - 1), right);
      --tour;
      right = right ^ feistel(second_key_gen(primary, tour - 1), left);
      --tour;
    }
  left_tmp = left;
  right_tmp = right;
  ret = (left_tmp << 32) | right_tmp;
  return (ret);
}

unsigned long long int		algo_loop_encrypt(long long int primary,
						  long long int *block)
{
  unsigned int			left;
  unsigned int			right;
  unsigned long long int	left_tmp;
  unsigned long long int	right_tmp;
  int				tour;
  unsigned long long int	ret;

  tour = 1;
  left = *((int *)block);
  right = *((int *)block + 1);
  while (tour <= 7)
    {
      left = left ^ feistel(second_key_gen(primary, tour - 1), right);
      ++tour;
      right = right ^ feistel(second_key_gen(primary, tour -1), left);
      ++tour;
    }
  left_tmp = right;
  right_tmp = left;
  ret = (left_tmp << 32) | right_tmp;
  return (ret);
}

int				algo_exec(t_data *data, long long int primary)
{
  int				ret;
  char				buffer[9];
  char				*rev;
  unsigned long long int	salamandre;

  while ((ret = read(data->infd, buffer, 8)) > 0)
    {
      buffer[ret] = 0;
      rev = revstr(buffer);
      if (ret != 8)
	put_padding();
      if (data->method == ENCRYPT)
	salamandre = algo_loop_encrypt(primary, (long long int *)rev);
      else
	salamandre = algo_loop_decrypt(primary, (long long int *)rev);
      rev = ltc_tab(rev, salamandre);
      write(data->outfd, rev, ret);
    }
  return (1);
}
